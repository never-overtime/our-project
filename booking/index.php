<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width,minimum-scale=1">
		<title>Book your trip</title>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css">
		<script src="https://kit.fontawesome.com/d2f4537413.js" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="style.css">
	</head>
	<body>
		<form class="hotel-reservation-form" method="post" target="blank" action="https://formspree.io/f/mlekebbr">
			<h1><i class="far fa-calendar-alt"></i>Tour Booking Form</h1>
			<div class="fields">
				<!-- Input Elements -->
				<div class="wrapper">
	<div>
		<label for="arrival">From</label>
		<div class="field">
			<input id="arrival" type="date" name="from" required>
		</div>
	</div>
	<div class="gap"></div>
	<div>
		<label for="departure">To</label>
		<div class="field">
			<input id="departure" type="date" name="to" required>
		</div>
	</div>
</div>
<div class="wrapper">
	<div>
		<label for="first_name">First Name</label>
		<div class="field">
			<i class="fas fa-user"></i>
			<input id="first_name" onkeyup="this.value = this.value.toUpperCase();" type="text" name="first_name" placeholder="First Name" required>
		</div>
	</div>
	<div class="gap"></div>
	<div>
		<label for="last_name">Last Name</label>
		<div class="field">
			<i class="fas fa-user"></i>
			<input id="last_name" type="text" onkeyup="this.value = this.value.toUpperCase();" name="last_name" placeholder="Last Name" required>
		</div>
	</div>
</div>
<label for="email">Email</label>
<div class="field">
	<i class="fas fa-envelope"></i>
	<input id="email" type="email" name="email" placeholder="Your Email" required>
</div>
<label for="phone">Phone</label>
<div class="field">
	<i class="fas fa-phone"></i>
	<input id="phone" type="tel" name="phone"  onkeypress="return onlyNumberKey(event)" maxlength="10" placeholder="Your Phone Number" required>
</div>
<label for="destination">Destination</label>
<div class="field">
<i class="fa-solid fa-location-dot"></i>
	<input id="location" type="tel" name="destination" onkeyup="this.value = this.value.toUpperCase();"   placeholder="type your destination" required>
</div>
<div class="wrapper">
	<div>
		<label for="adults">Adults</label>
		<div class="field">
			<select id="adults" name="adults" required>
				<option disabled selected value="">--</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
			</select>
		</div>
	</div>
	<div class="gap"></div>
	<div>
		<label for="children">Children</label>
		<div class="field">
			<select id="children" name="children" required>
				<option disabled selected value="">--</option>
				<option value="0">0</option>
				<option value="1">1</option>
				<option value="2">2</option>
				<option value="3">3</option>
				<option value="4">4</option>
			</select>
		</div>
	</div>
</div>

<label for="room_pref">Class Preference</label>
<div class="field">
	<select id="room_pref" name="class reference" required>
		<option disabled selected value="">--</option>
		<option value="Standard">Standard</option>
		<option value="Deluxe">Deluxe</option>
		<option value="Suite">Suite</option>
	</select>
</div>
<input type="submit" value="Book">
			</div>
		</form>
		<script>

  function onlyNumberKey(evt) {
  // Only ASCII character in that range allowed
  var ASCIICode = (evt.which) ? evt.which : evt.keyCod
  if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
  return false;
  return true;
  }
  </script>
 
	</body>
</html>